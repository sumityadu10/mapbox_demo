import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'modals/MapRoute.dart';
class DatabaseService {
  // Singleton pattern
  static final DatabaseService _databaseService = DatabaseService._internal();
  factory DatabaseService() => _databaseService;
  DatabaseService._internal();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    // Initialize the DB first time it is accessed
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final databasePath = await getDatabasesPath();
    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    final path = join(databasePath, 'mapbox_database.db');

    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    return await openDatabase(path, onCreate: _onCreate, version: 1);
  }
  Future<void> _onCreate(Database db, int version) async {
    // This is used to add/remove Column
    // db.execute("ALTER TABLE tabEmployee ADD COLUMN newCol TEXT;");
    await db.execute("DROP TABLE IF EXISTS routes");
    await db.execute(
      'CREATE TABLE routes(id INTEGER PRIMARY KEY AUTOINCREMENT,createdAt TEXT, latitude DOUBLE, longitude DOUBLE)',
    );
    await db.execute(
      'CREATE TABLE punches(id INTEGER PRIMARY KEY, isIn INTEGER)',
    );
    initializeUser(11);
    // Run the CREATE {dogs} TABLE statement on the database.
    // await db.execute(
    //   'CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER, color INTEGER, breedId INTEGER, FOREIGN KEY (breedId) REFERENCES breeds(id) ON DELETE SET NULL)',
    // );
  }
  Future<bool> isPunchIn(int id) async {
    final db = await _databaseService.database;
    final List<Map<String, dynamic>> maps = await db.query('punches');
    int isIn = maps.first['isIn'];
    return isIn == 1 ? true : false;
  }
  Future<void> punchOut(int id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;
    Map<String, dynamic> map =  {
      'id': id,
      'isIn': '0',
    };
    // Update the given breed
    await db.update(
      'punches',
      map,
      // Ensure that the Breed has a matching id.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> punchIn(int id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;
    Map<String, dynamic> map =  {
      'id': id,
      'isIn': '1',
    };
    // Update the given breed
    await db.update(
      'punches',
      map,
      // Ensure that the Breed has a matching id.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  // Define a function that inserts breeds into the database
  Future<void> insertBreed(MapRoute breed) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Insert the Breed into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same breed is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'routes',
      breed.toMap(),
      // conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  // A method that updates a breed data from the breeds table.
  Future<void> updateBreed(MapRoute mapRoute) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Update the given breed
    await db.update(
      'routes',
      mapRoute.toMap(),
      // Ensure that the Breed has a matching id.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      // whereArgs: [mapRoute.id],
    );
  }
// A method that deletes a breed data from the breeds table.
  Future<void> deleteRoute(int? id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Remove the Breed from the database.
    await db.delete(
      'routes',
      // Use a `where` clause to delete a specific breed.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
  Future<void> initializeUser(int id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;
    Map<String, dynamic> map =  {
      'id': id,
      'isIn': '0',
    };
    // Update the given breed
    await db.insert(
      'punches',
      map,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  // A method that retrieves all the breeds from the breeds table.
  Future<List<MapRoute>> fetchRoutes() async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Query the table for all the Breeds.
    final List<Map<String, dynamic>> maps = await db.query('routes');
    // Convert the List<Map<String, dynamic> into a List<Breed>.
    return List.generate(maps.length, (index) => MapRoute.fromMap(maps[index]));
  }
}