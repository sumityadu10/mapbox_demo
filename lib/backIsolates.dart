import 'dart:async';
import 'dart:developer';
import 'dart:isolate';

import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';

import 'modals/MapRoute.dart';

Isolate? currentIsolate;
final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;
StreamSubscription<Position>? _positionStreamSubscription;
Future initializeIsolate() async {
  RootIsolateToken rootIsolateToken = RootIsolateToken.instance!;
  ReceivePort isolateToMainStream = ReceivePort();
  // isolateToMainStream.listen((message) {
  //   if (kDebugMode) {
  //     print('New isolateToMainStream Message ================== $message');
  //   }
  // });
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  AndroidNotificationDetails androidPlatformSpecificChannel =
  const AndroidNotificationDetails(
      'tracker_1',
      'tracker',
      playSound: true,
      importance: Importance.defaultImportance,
      priority: Priority.high
  );
  var notificationDetails = NotificationDetails(
      android: androidPlatformSpecificChannel,
      iOS: const DarwinNotificationDetails()
  );
  // ReceivePort isolateStream = ReceivePort();
  await Isolate.spawn(onLocationUpdated, [isolateToMainStream.sendPort,rootIsolateToken]);
  isolateToMainStream.listen((message) {
    Position position = message;
    log('New Location on Main Isolate ============================== ${position.longitude}');
    var now  = DateTime.now();
    //Month is capital MM to distinct 'minute and month'
    //HH means 24 hour format
    var formatter  = DateFormat('yyyy-MM-dd HH:mm:ss');
    var route = MapRoute(createdAt:formatter.format(now),latitude: position.latitude, longitude: position.longitude);
    // await isolatedDbService.insertBreed(route);
    flutterLocalNotificationsPlugin.show(
      0,
      'New Location Caught',
      formatter.format(now) + position.latitude.toString() + position.longitude.toString(),
      notificationDetails,
    );
  });
}

@pragma('vm:entry-point')
void onLocationUpdated(List<dynamic> args)  async{
  SendPort isolateToMainStream = args[0];
  RootIsolateToken rootIsolateToken = args[1];
  BackgroundIsolateBinaryMessenger.ensureInitialized(rootIsolateToken);
  ReceivePort mainToIsolateStream =  ReceivePort();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  if (_positionStreamSubscription == null) {
    final positionStream = _geolocatorPlatform.getPositionStream(
      locationSettings: const LocationSettings(
          accuracy: LocationAccuracy.high,
          distanceFilter: 0,
          timeLimit: Duration(seconds: 5)
      ),
    );
    _positionStreamSubscription = positionStream.listen((position)
    {
      isolateToMainStream.send(position);
    });
  }

  // mainToIsolateStream.listen((message) {
  //   if (kDebugMode) {
  //     print('New Message ================== $message');
  //   }
  // });
}