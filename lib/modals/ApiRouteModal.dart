class ApiRouteModal{
  String? status;
  String? message;
  List<ApiRoute>? apiRoutes;
  ApiRouteModal({this.status,this.message,this.apiRoutes});
  factory ApiRouteModal.fromJson(Map<String, dynamic> json) => ApiRouteModal(
    status: json["status"],
    message: json["message"],
    apiRoutes: List<ApiRoute>.from(
        json["list_array"].map((x) => ApiRoute.fromJson(x))),
  );
}

class ApiRoute{
  String? lat;
  String? lng;
  ApiRoute({this.lat,this.lng});

  factory ApiRoute.fromJson(Map<String, dynamic> json) => ApiRoute(
    lat: json["lat"],
    lng: json["lng"],
  );
}