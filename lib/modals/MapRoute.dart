class MapRoute{
  final int? id;
  final String? createdAt;
  final double? latitude;
  final double? longitude;

  MapRoute({
    this.id,
    required this.createdAt,
    required this.latitude,
    required this.longitude
  });
  // Convert a Breed into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      // 'id': id,
      'createdAt': createdAt,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory MapRoute.fromMap(Map<String, dynamic> map) {
    return MapRoute(
      id: map['id']?.toInt() ?? 0,
      createdAt: map['createdAt'] ?? '',
      latitude: map['latitude'] ?? '',
      longitude: map['longitude'] ?? '',
    );
  }

  // Implement toString to make it easier to see information about
  // each breed when using the print statement.
  @override
  String toString() => 'Breed(id: $id,createdAt: $createdAt, latitude: $latitude, longitude: $longitude)';
}