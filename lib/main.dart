import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:disable_battery_optimization/disable_battery_optimization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:mapbox_demo/backgoundTrackingService.dart';
import 'package:mapbox_demo/shared_preferences/constants.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mapbox_demo/modals/MapRoute.dart';
import 'package:mapbox_demo/previousRoutesPage.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'database_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeService();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Map Box Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Map Box Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver  {
  AppLifecycleState? appLifeCycleState = AppLifecycleState.resumed;
  final DatabaseService _databaseService = DatabaseService();
  final BackgroundTrackingService backgroundService = BackgroundTrackingService();
  LatLng currentLocation = const LatLng(0.0,0.0);
  List<LatLng> myroutes = [];
  bool isRunning = false;
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  bool _isIn = false;
  MapboxMapController? _mapController;
  List<LatLng> positions = [];
  void getCurrentLocation() async {
    var locate = await Geolocator.getCurrentPosition();
    currentLocation = LatLng(locate.latitude, locate.longitude);
  }
  CameraPosition getCurrentPosition() {
    return CameraPosition(
      target: currentLocation,
      zoom: 11.0,);
  }

  void submitRoutesToApi(List<MapRoute> dbRoutes) async {
    if(dbRoutes.isEmpty) {
      return;
    }
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.contentType = 'multipart/form-data';

      var deviceInfo = await DeviceInfoPlugin().androidInfo;
      List<String> routes = [];

      for (var element in dbRoutes) {
        Map<String, String> positionMap = {
          '"lat"': '"${element.latitude.toString()}"',
          '"lng"': '"${element.longitude.toString()}"',
          '"created_at"': '"${element.createdAt!}"',
        };
        routes.add(positionMap.toString());
      }

      FormData formData =
      FormData.fromMap({'emp_id': deviceInfo.id, 'list': routes.toString()});
      debugPrint(formData.fields.toString());
      Response response = await dio.post(
          'https://timekompas.com/api/shyam/save-live-location-test',
          data: formData);
      if(response.statusCode == 200){
        // showDonePopup('Data Submitted Successfully');
      }
    } catch (e) {
      // showDonePopup('Something went wrong');
    }
  }
  Future _updateUI() async {
    // _lastLocation = data;

    if(myroutes.length > 2 ){
      // myroutes.addAll(positions);
      myroutes = [];
      _mapController!.addLine(
        LineOptions(
            geometry: myroutes,
            lineColor: "#0096FF",
            lineWidth: 8.0,
            lineOpacity: 0.5,
            draggable: false),
      );
    }
  }
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Future disableBatteryOptimization() async {
    if(Platform.isAndroid){
      var deviceInfo = await deviceInfoPlugin.androidInfo;
      if(deviceInfo.version.sdkInt < 29){
        var isDisabled = await DisableBatteryOptimization.isAllBatteryOptimizationDisabled;
        if(!isDisabled!){
          DisableBatteryOptimization.showDisableAllOptimizationsSettings('Timekompas Process', 'Enable Timekompas Battery Usage', 'Battery Optimization', 'Enable process');
        }
      }
    }
  }
  Future<bool> _checkLocationPermission() async {
    final isGranted = await Permission.location.isGranted;
    if(!isGranted){
      final permission = await Permission.locationWhenInUse.request();
      if (permission.isGranted) {
        final permissionAlways = await Permission.locationAlways.request();
        if(permissionAlways.isGranted){
          return true;
        }
        return true;
      } else {
        return false;
      }
    }
    getCurrentPosition();
    return isGranted;
  }

  @override
  void dispose() {
    super.dispose();
    _mapController!.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
      appLifeCycleState = state;
      // if(appLifeCycleState == AppLifecycleState.detached){
      //
      // }
  }

  punchIn() async {
    await _databaseService.punchIn(11);
    var isRunning = await backgroundService.isRunning();
    if (!isRunning) {
      await backgroundService.startService();
      isRunning = await backgroundService.isRunning();
      log('Is Locator Running: $isRunning');
    }
    else{
      backgroundService.stopService();
    }
    setState(() {
      _isIn = true;
    });
  }

  punchOut() async {
    backgroundService.stopService();
    var isRunning = await backgroundService.isRunning();
    if(isRunning){
      log('Is Locator Running: $isRunning');
      backgroundService.stopService();
    }
    await _databaseService.punchOut(11);
    setState(() {
      _isIn = false;
    });

    var dbRoutes = await _databaseService.fetchRoutes();
    submitRoutesToApi(dbRoutes);
    if(dbRoutes.isNotEmpty){
      for (var element in dbRoutes) {
        _databaseService.deleteRoute(element.id);
      }
    }
  }
  checkPunch() async{
    var isStillIn = await _databaseService.isPunchIn(11);
    if(!isStillIn){
      backgroundService.stopService();
    }
    setState(() {
      _isIn = isStillIn;
    });
  }


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    disableBatteryOptimization();
    _checkLocationPermission();
    checkPunch();
    //Listens for the changes and updates UI
  }

  _onMapCreated(MapboxMapController controller) {
    _mapController = controller;
    getCurrentLocation();
  }

  Future<bool> showExitPopup() async {
    return await showDialog(
      //show confirm dialogue
      //the return value will be from "Yes" or "No" options
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Location Service'),
        content: const Text('Location Service will be terminated'),
        actions:[
          ElevatedButton(
            onPressed: () => Navigator.of(context).pop(true),
            //return false when click on "NO"
            child:const Text('Ok'),
          ),
          ElevatedButton(
            onPressed: () => Navigator.of(context).pop(false),
            //return false when click on "NO"
            child:const Text('Cancel'),
          ),
        ],
      ),
    )??false; //if showDialouge had returned null, then return false
  }
  showDonePopup(String response) async {
    return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        icon: CircleAvatar(
            child: Icon(Icons.done,color: Colors.green[300],size: 28,)),
        title: const Text('Success'),
        content:  Text(response),
        actions:[
          ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            //return false when click on "NO"
            child:const Text('Ok'),
          ),
        ],
      ),
    ); //if showDialouge had returned null, then return false
  }
  _onStyleLoadedCallback() {

  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future(() => true), //showExitPopup(),
      child: Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (builder){
                  return const PreviousRoutesPage();
                }));
          },
          label: const Text('View Previous Tracking'),
        ),
        appBar: AppBar(
          title: const Text('Map Box',style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w800
          ),),
          actions: [
            !_isIn ?  Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(onPressed: () {
                // sendNotification('Hello from another world','Hello');
                punchIn();
              }, child: const Text('Punch In')),
            )
                : Padding(
              padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(onPressed: () => punchOut(),
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.purple[100],
                  ),
                  child: const Text('Punch Out')
              ),
            ),
          ],
          backgroundColor: Colors.white,
          foregroundColor: Colors.cyan,
        ),
        body: MapboxMap(
          styleString: MapboxStyles.MAPBOX_STREETS,
          accessToken: mapboxPublicToken,
          onMapCreated: _onMapCreated,
          minMaxZoomPreference: const MinMaxZoomPreference(11,18),
          initialCameraPosition: const CameraPosition(
              target:LatLng(21.2442326,81.6109523),
              zoom: 11.0,
              bearing: 10.0),
          onStyleLoadedCallback: _onStyleLoadedCallback,
            )
        ),
    );
  }
}