import 'package:device_info_plus/device_info_plus.dart';
import 'package:geolocator/geolocator.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mapbox_demo/modals/ApiRouteModal.dart';
import 'package:mapbox_demo/shared_preferences/constants.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:toast/toast.dart';

import 'modals/MapRoute.dart';

class PreviousRoutesPage extends StatefulWidget {
  const PreviousRoutesPage({Key? key}) : super(key: key);

  @override
  State<PreviousRoutesPage> createState() => _PreviousRoutesState();
}

class _PreviousRoutesState extends State<PreviousRoutesPage> {
  DateTime? selectedDate = DateTime.now();
  Widget? mapboxWidget = Container();
  LatLng selectedRouteInitialPosition = const LatLng(0.0, 0.0);
  final LatLng currentLocation = const LatLng(0.0,0.0);
  double lat = 0.0;
  double lng = 0.0;

  List<LatLng> lineRoutes = [];
  MapRoute? selectedMapRoute;
  MapboxMapController? mapController;
  _onMapCreated(MapboxMapController mapBoxController){
    mapController = mapBoxController;
    getCurrentLocation();
  }
  void _onStyleLoadedCallback() {

  }
  void getCurrentLocation() async {
    var locate = await Geolocator.getCurrentPosition();
    lat = locate.latitude;
    lng = locate.longitude;
    // var currentLocation = LatLng(locate.latitude, locate.longitude);
  }
  CameraPosition getCurrentPosition() {
    return CameraPosition(
      target: currentLocation,
      zoom: 11.0,);
  }
  Future<bool> dataFetched() async {
    return await showDialog(
      //show confirm dialogue
      //the return value will be from "Yes" or "No" options
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Location Service'),
        content: const Text('Data fetched from server'),
        actions:[
          ElevatedButton(
            onPressed: () => Navigator.of(context).pop(false),
            //return false when click on "NO"
            child:const Text('Ok'),
          ),
        ],
      ),
    )??false; //if showDialouge had returned null, then return false
  }

  Future getData(String date) async {
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      // dio.options.contentType = 'multipart/form-data';

      var deviceInfo = await DeviceInfoPlugin().androidInfo;
      FormData formData = FormData.fromMap({
        'token_id' : deviceInfo.id,
        'date' : date
      });
      Response response = await dio.post('https://timekompas.com/api/shyam/getusertrackinghistory',data:formData);
      if (kDebugMode) {
        print(formData.fields.toString());
      }
      showDonePopup(response.toString());
      if(response.statusCode == 200){
        var apiModal = ApiRouteModal.fromJson(response.data);
        var apiRoutes = apiModal.apiRoutes;
        // await dataFetched();
        lineRoutes.clear();
        for(var routes in apiRoutes!){
          lineRoutes.add(LatLng(double.parse(routes.lat!), double.parse(routes.lng!)));
        }
      setState((){
        selectedRouteInitialPosition = lineRoutes[0];
        mapController!.clearLines();
        mapController!.addLine(
          LineOptions(
              geometry: lineRoutes,
              lineColor: "#0096FF",
              lineWidth: 8.0,
              lineOpacity: 0.8,
              draggable: false),
        );
      });
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }
  Future<bool> showDonePopup(String response) async {
    return await showDialog(
      //show confirm dialogue
      //the return value will be from "Yes" or "No" options
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Success'),
        content:  Text(response),
        actions:[
          ElevatedButton(
            onPressed: () => Navigator.of(context).pop(false),
            //return false when click on "NO"
            child:const Text('Ok'),
          ),
        ],
      ),
    )??false; //if showDialouge had returned null, then return false
  }
  @override
  void initState() {
    // TODO: implement initState
    getCurrentLocation();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Previous Records'),
      ),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
            ),
            padding: const EdgeInsets.all(8.0),
              child: Card(
                    child: ListTile(
                        onTap: () async {
                          selectedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2021),
                              lastDate: DateTime.now());
                          // setState(() {});
                          var formatter = DateFormat('yyyy-MM-dd');
                          var date = formatter.format(selectedDate!);
                          getData(date);
                        },
                        leading: const Icon(Icons.calendar_month_sharp,color: Colors.grey,),title: Text(selectedDate.toString().substring(0,10)))),
              ),
          Flexible(
            flex: 5,
              child: MapboxMap(
                styleString: MapboxStyles.MAPBOX_STREETS,
                accessToken: mapboxPublicToken,
                onMapCreated: _onMapCreated,
                minMaxZoomPreference: const MinMaxZoomPreference(11,20),
                initialCameraPosition: const CameraPosition(
                  target:LatLng(21.2442326,81.6109523),
                  zoom: 11.0,
                bearing: 10.0),
                onStyleLoadedCallback: _onStyleLoadedCallback,
                dragEnabled: true,
              )

          )
        ],
      ),
    );
  }
}
