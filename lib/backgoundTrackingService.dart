import 'dart:async';
import 'dart:developer';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:mapbox_demo/database_service.dart';
import 'package:mapbox_demo/modals/MapRoute.dart';

const notificationChannelId = 'my_foreground';
const notificationId = 888;
final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;
StreamSubscription<ServiceStatus>? _serviceStatusStreamSubscription;
StreamSubscription<Position>? _positionStreamSubscription;
FlutterBackgroundService backgroundService = FlutterBackgroundService();

Future<void> initializeService() async{
  final service = FlutterBackgroundService();
  await service.configure(
      androidConfiguration: AndroidConfiguration(
        // this will be executed when app is in foreground or background in separated isolate
        onStart: onStart,

        // auto start service
        autoStart: true,
        isForegroundMode: true,
        notificationChannelId: notificationChannelId, // this must match with notification channel you created above.
        // initialNotificationTitle: 'AWESOME SERVICE',
        // initialNotificationContent: 'Initializing',
        foregroundServiceNotificationId: notificationId,
      ),
      iosConfiguration: IosConfiguration()
  );
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
    notificationChannelId, // id
    'MY FOREGROUND SERVICE', // title
    description:
    'This channel is used for important notifications.', // description
    importance: Importance.low, // importance must be at low or higher level
  );

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
}
@pragma('vm:entry-point')
Future<void> onStart(ServiceInstance service) async {
  // Only available for flutter 3.0.0 and later
  DartPluginRegistrant.ensureInitialized();
  if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });

    service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
    service.on('stopService').listen((event) async{
      service.stopSelf();
      _positionStreamSubscription?.cancel();
      _positionStreamSubscription = null;
    });
  }
  final DatabaseService databaseService = DatabaseService();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  // if (_serviceStatusStreamSubscription == null) {
  //   final serviceStatusStream = _geolocatorPlatform.getServiceStatusStream();
  //   _serviceStatusStreamSubscription =
  //       serviceStatusStream.listen((serviceStatus) {
  //         if (serviceStatus == ServiceStatus.enabled) {
  //         } else {
  //           if (_positionStreamSubscription != null) {
  //               _positionStreamSubscription?.cancel();
  //               _positionStreamSubscription = null;
  //           }
  //         }
  //       },
  //       onError: (error){
  //         _serviceStatusStreamSubscription?.cancel();
  //         _serviceStatusStreamSubscription = null;
  //       }
  //       );
  // }
  if (_positionStreamSubscription == null) {
    final positionStream = _geolocatorPlatform.getPositionStream(
      locationSettings: const LocationSettings(
          accuracy: LocationAccuracy.high,
          distanceFilter: 2,
      ),
    );
    if(await databaseService.isPunchIn(11)){
      _positionStreamSubscription = positionStream.listen((position) async {
        if (kDebugMode) {
          print('Position : $position');
        }
        flutterLocalNotificationsPlugin.show(
          notificationId,
          'COOL SERVICE',
          position.latitude.toString() + position.longitude.toString() + DateTime.now().toString(),
          const NotificationDetails(
            android: AndroidNotificationDetails(
              notificationChannelId,
              'MY FOREGROUND SERVICE',
              icon: 'ic_bg_service_small',
              ongoing: true,
            ),
          ),
        );
        var now  = DateTime.now();
        //Month is capital MM to distinct 'minute and month'
        //HH means 24 hour format
        var formatter  = DateFormat('yyyy-MM-dd HH:mm:ss');
        var route = MapRoute(createdAt:formatter.format(now),latitude: position.latitude, longitude: position.longitude);
        try {
          await databaseService.insertBreed(route);
          log('Data Submitted createdAt: ${formatter.format(now)},latitude: ${position.latitude} longitude: ${position.longitude}');
        } catch (e) {
          if (kDebugMode) {
            // print(e);
          }
        }
      });
    }
  }
}

class BackgroundTrackingService{
  Future<bool> isRunning() async => await backgroundService.isRunning();

  Future<void> startService() async => await backgroundService.startService();
  stopService() => backgroundService.invoke('stopService');
}